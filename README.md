# property-reporting

## Running

```
sudo apt install libwebkit2gtk-4.0-dev     build-essential     curl     wget     file     libssl-dev     libgtk-3-dev     libayatana-appindicator3-dev     librsvg2-dev
cargo install tauri-cli
cargo install diesel_cli --no-default-features --features postgres
```

Install rustup:

```
rustup update
npm install
```

on Ubuntu:
```
unset GTK_PATH
```

To run you must run migrations against a postgis db, use docker-compose to bring it up
```
docker compose up -d
cat "127.0.0.1       pgadmin.pr.local traefik.pr.local" >> /etc/hosts
diesel migration run --database-url=postgres://postgis:djhfdahf23215@127.0.0.1
DATABASE_URL=postgres://postgis:djhfdahf23215@127.0.0.1 cargo tauri dev
```

Data Sources:

Seems to be some good shape files here: https://www.abs.gov.au/statistics/standards/australian-statistical-geography-standard-asgs-edition-3/jul2021-jun2026/access-and-downloads/digital-boundary-files#downloads-for-gda2020-digital-boundary-files

```
export PGPASSWORD=... // Probably
ogr2ogr -nln SA4_2021_AUST_GDA2020 -nlt PROMOTE_TO_MULTI -lco GEOMETRY_NAME=geom -lco FID=gid -lco PRECISION=NO Pg:"dbname=postgis host=127.0.0.1 user=postgis port=5432" ~/Downloads/SA4_2021_AUST_SHP_GDA2020/SA4_2021_AUST_GDA2020.shp
```


Reingest static data:
```
diesel migration redo -n 9 --database-url=postgres://postgis:djhfdahf23215@127.0.0.1
// Delete rows from ingestion
DATABASE_URL=postgres://postgis:djhfdahf23215@127.0.0.1 cargo test
diesel migration redo -n 1 --database-url=postgres://postgis:djhfdahf23215@127.0.0.1
```