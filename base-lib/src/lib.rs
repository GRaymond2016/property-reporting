#[macro_use]
extern crate diesel;

pub mod data_schema;

use wasm_bindgen::prelude::wasm_bindgen;
use serde::{Serialize, Deserialize};
pub use crate::data_schema::*;

#[wasm_bindgen(getter_with_clone)]
#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct AddressLookupResult {
    pub address: String,
    pub latitude: f64,
    pub longitude: f64
}

pub fn sample_listing_data() -> Vec<u8> {
    return include_bytes!("test_data/listing2.json").to_vec();
}

#[cfg(test)]
mod tests {
    use crate::data_schema::listing_structs::Listing;

    #[test]
    fn test_parsing() {
        let example = String::from_utf8(include_bytes!("test_data/listing.json").to_vec())
            .expect("Not utf8 string");
        let rs = serde_json::from_str::<Vec<Listing>>(example.as_str())
            .expect("Could not decode listings");
        let example2 = String::from_utf8(include_bytes!("test_data/listing2.json").to_vec())
            .expect("Not utf8 string");
        let _rs2 = serde_json::from_str::<Vec<Listing>>(example2.as_str())
            .expect("Could not decode listings");

        assert_eq!(rs.len(), 2);
        let ex = rs.first().expect("Could not iterate");
        assert_eq!(
            "177-australia-street-newtown-nsw-2042-2013958589",
            ex.listing.listing_slug
        );
        assert_eq!(2.0, ex.listing.property_details.bathrooms);
        assert_eq!(
            "2017-11-01T17:30:00",
            ex.listing
                .inspection_schedule.as_ref()
                .unwrap()
                .times
                .first()
                .expect("No inspection times")
                .opening_time
        );
    }
}