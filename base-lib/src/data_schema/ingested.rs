use diesel::prelude::*;

#[derive(Insertable)]
#[diesel(table_name = ingested)]
pub struct IngestedInsert {
    pub unique_source_name: String,
    pub data_hash: String,
}

#[derive(Queryable, Selectable)]
#[diesel(table_name = ingested)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct IngestedQuery {
    pub id: i32,
    pub unique_source_name: String,
    pub data_hash: String,
}

table! {
    use postgis_diesel::sql_types::{Geometry};
    use diesel::sql_types::*;

    ingested (id) {
        id -> Int4,
        unique_source_name -> Text,
        data_hash -> Text
    }
}
