use geojson::{GeoJson, FeatureCollection, feature::Id};
use serde_json::{Map, Value};
use simple_error::bail;
use diesel::sql_types::*;
use crate::geo_models::SuburbsInsert;

sql_function! {
    #[sql_name="ST_GeomFromGeoJSON"]
    fn st_geom_from_geo_json(geomjson: Text) -> postgis_diesel::sql_types::Geometry;
}

pub fn convert_feature_collection_to_suburb_inserts(input: &str) -> Result<Vec<SuburbsInsert>, Box<dyn std::error::Error>> {
    let geojson: GeoJson = input.parse::<GeoJson>()?;
    let coll: FeatureCollection = FeatureCollection::try_from(geojson)?;
    let inserts = coll.features.iter().map(|feature| -> Result<SuburbsInsert, Box<dyn std::error::Error>> {
        if let Some(x) = &feature.geometry {
            let id = match &feature.id {
                Some(Id::String(x)) => x.clone(),
                Some(Id::Number(x)) => format!("{}", x.to_string()),
                None => {
                    log::warn!("Id property does not exist");
                    bail!("Id");
                }
            };
            let props = match feature.properties.clone() {
                Some(x) => x,
                None => {
                    log::warn!("Id property does not exist");
                    Map::<String, Value>::new()
                }
            };
            let props_val = match serde_json::from_value(props.into()) {
                Ok(x) => x,
                Err(e) => {
                    log::warn!("Could not encode props: {}", e);
                    bail!("props");
                }
            };
            return Ok(SuburbsInsert {
                name: id.to_string(),
                outline: st_geom_from_geo_json(x.to_string()),
                properties: props_val
            });
        }
        return Err(Box::new(simple_error::simple_error!("No geometry here")));
    }).filter(|x| x.is_ok()).map(|x| x.unwrap()).collect();
    return Ok(inserts);
}

#[cfg(test)]
mod tests {
    use crate::conversions::convert_feature_collection_to_suburb_inserts;
    
    #[test]
    fn test_parsing() {
        let example = String::from_utf8(include_bytes!("../../data/suburb-10-nsw.geojson").to_vec())
            .expect("Not utf8 string");
        assert!(convert_feature_collection_to_suburb_inserts(example.as_str()).unwrap().len() > 0);
    }
}