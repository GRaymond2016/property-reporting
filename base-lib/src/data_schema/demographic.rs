pub struct SuburbPriceData {
    pub median_sale_price_house: i32,
    pub median_gross_yield: f32,
    pub median_price_change_1y_percent: f32,
    pub average_days_on_market: i32,
    pub properties_sold: i32,
    pub average_discounting: i32,
    pub median_rent: i32,
}

pub struct DemographicInfo {
    pub less_than_338k_household_income_percent: i32,
    pub greater_than_130k_household_income_percent: i32,
    pub home_owner_occupancy_percent: i32,
    pub couples_with_children_percent: i32,
}
