use diesel::internal::derives::as_expression::Bound;

use postgis_diesel::types::{Polygon, Point};

use crate::geo_schema::{suburb_prices, suburbs};
use crate::conversions::st_geom_from_geo_json::st_geom_from_geo_json;

#[derive(Insertable)]
#[diesel(table_name = suburbs)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct SuburbsInsert {
    pub name: String,
    pub outline: st_geom_from_geo_json<Bound<diesel::sql_types::Text, std::string::String>>,
    pub properties: diesel_json::Json<serde_json::Map<String, serde_json::Value>>,
}

#[derive(Queryable, Selectable, Identifiable, Debug, PartialEq)]
#[diesel(table_name = suburbs)]
pub struct SuburbsQuery {
    pub id: i32,
    pub name: String,
    pub outline: Polygon<Point>,
    pub center: Point,
    pub properties: diesel_json::Json<serde_json::Map<String, serde_json::Value>>,
}

#[derive(Queryable, Selectable, Identifiable, Associations, Debug, PartialEq)]
#[diesel(table_name = suburb_prices)]
#[diesel(belongs_to(SuburbsQuery))]
pub struct SuburbPricesQuery {
    pub id: i32,
    pub suburbs_query_id: i32,
    pub suburb_name: String,
    pub postcode: Option<String>,
    pub median_house_price: Option<i32>,
    pub median_unit_price: Option<i32>,
    pub year: Option<i32>,
    pub month: Option<i32>
}