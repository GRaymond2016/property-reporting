sql_function! {
    #[sql_name="ST_Centroid"]
    fn st_centroid(geomjson: postgis_diesel::sql_types::Geometry) -> postgis_diesel::sql_types::Geometry;
}

table! {
    use postgis_diesel::sql_types::{Geometry};
    use diesel::sql_types::*;

    suburbs (id) {
        id -> Int4,
        name -> Text,
        outline -> Geometry,
        center -> Geometry,
        properties -> Jsonb
    }
}

table! {
    use postgis_diesel::sql_types::{Geometry};
    use diesel::sql_types::*;

    suburb_prices (id) {
        id -> Int4,
        suburbs_query_id -> Int4,
        suburb_name -> Text,
        postcode -> Nullable<Text>,
        median_house_price -> Nullable<Int4>,
        median_unit_price -> Nullable<Int4>,
        year -> Nullable<Int4>,
        month -> Nullable<Int4>
    }
}

diesel::joinable!(suburb_prices -> suburbs (suburbs_query_id));

diesel::allow_tables_to_appear_in_same_query!(
    suburbs,
    suburb_prices,
);