use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct InspectionTime {
    pub opening_time: String,
    pub closing_time: String,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct InspectionSchedule {
    pub by_appointment: bool,
    pub times: Vec<InspectionTime>,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct AuctionSchedule {
    pub time: String,
    pub auction_location: String,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct PropertyDetails {
    pub state: String,
    pub property_type: String,
    pub bedrooms: f32,
    pub bathrooms: f32,
    pub latitude: f64,
    pub longitude: f64,
    pub land_area: Option<f32>,
    pub building_area: Option<f32>,
    pub unit_number: String,
    pub street_number: String,
    pub street: String,
    pub area: String,
    pub suburb: String,
    pub postcode: String,
    pub displayable_address: String,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct PriceDetails {
    pub display_price: String,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct PropertyListing {
    pub listing_type: String,
    pub id: i32,
    pub headline: String,
    pub price_details: PriceDetails,
    pub property_details: PropertyDetails,
    pub auction_schedule: Option<AuctionSchedule>,
    pub inspection_schedule: Option<InspectionSchedule>,
    pub listing_slug: String,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Listing {
    #[serde(rename = "type")]
    pub listing_type: String,
    pub listing: PropertyListing,
}

impl PropertyDetails {
    pub fn get_address_string(&self) -> String {
        String::from(self.street_number.clone()) + " " + &self.street + " " + &self.suburb + " " + &self.state
    }
}