use chrono::NaiveDateTime;
use crate::sale_data_schema::suburb_sales;

#[derive(Insertable)]
#[diesel(table_name = suburb_sales)]
pub struct NewSuburbPrices {
    pub district_code: String,
    pub property_id: String,
    pub sale_counter: String,
    pub property_name: String,
    pub property_unit_number: String,
    pub property_house_number: String,
    pub property_street_name: String,
    pub property_locality: String,
    pub property_post_code: String,
    pub area: String,
    pub area_type: String,
    pub zoning: String,
    pub nature_of_property: String,
    pub primary_purpose: String,
    pub strata_lot_number: String,
    pub component_code: String,
    pub sale_code: String,
    pub percent_interest_of_sale: String,
    pub dealing_number: String,
    pub contract_date: NaiveDateTime,
    pub settlement_date: NaiveDateTime,
    pub download_datetime: NaiveDateTime,
    pub purchase_price: i64,
}

#[derive(Queryable, Selectable, Identifiable, Debug, PartialEq)]
#[diesel(table_name = suburb_sales)]
pub struct SuburbPricesQuery {
    pub id: i32,
    pub district_code: String,
    pub property_id: String,
    pub sale_counter: String,
    pub property_name: String,
    pub property_unit_number: String,
    pub property_house_number: String,
    pub property_street_name: String,
    pub property_locality: String,
    pub property_post_code: String,
    pub area: String,
    pub area_type: String,
    pub zoning: String,
    pub nature_of_property: String,
    pub primary_purpose: String,
    pub strata_lot_number: String,
    pub component_code: String,
    pub sale_code: String,
    pub percent_interest_of_sale: String,
    pub dealing_number: String,
    pub contract_date: NaiveDateTime,
    pub settlement_date: NaiveDateTime,
    pub download_datetime: NaiveDateTime,
    pub purchase_price: i64,
}