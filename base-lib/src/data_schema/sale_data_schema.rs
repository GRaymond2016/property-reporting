table! {
    use postgis_diesel::sql_types::{Geometry};
    use diesel::sql_types::*;

    suburb_sales (id) {
        id -> Int4,
        district_code -> Text,
        property_id -> Text,
        sale_counter -> Text,
        download_datetime -> Timestamp,
        property_name -> Text,
        property_unit_number -> Text,
        property_house_number -> Text,
        property_street_name -> Text,
        property_locality -> Text,
        property_post_code -> Text,
        area -> Text,
        area_type -> Text,
        contract_date -> Timestamp,
        settlement_date -> Timestamp,
        purchase_price -> Int8,
        zoning -> Text,
        nature_of_property -> Text,
        primary_purpose -> Text,
        strata_lot_number -> Text,
        component_code -> Text,
        sale_code -> Text,
        percent_interest_of_sale -> Text,
        dealing_number -> Text
    }
}