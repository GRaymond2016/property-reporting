use std::{collections::HashMap, rc::Rc};

use base_lib::listing_structs::Listing;
use geojson::{Feature, GeoJson, FeatureCollection};
use mapboxgl::{Map, LngLat, PopupOptions, MarkerOptions, Layer, layer::{self, LineLayerType}};

pub fn get_suburb_features() -> Result<Vec<Feature>, Box<dyn std::error::Error>> {
    let data = include_str!("../../base-lib/data/suburb-10-nsw.geojson");
    let geojson: GeoJson = data.parse::<GeoJson>()?;
    let feature_coll: FeatureCollection = FeatureCollection::try_from(geojson)?;
    return Ok(feature_coll.features);
}

pub fn render_suburbs(features: &Vec<Feature>, ids: &Vec<String>, map: &Rc<Map>) -> Result<Vec<String>, Box<dyn std::error::Error>> {
    let mut results = vec![];
    let mut i = 0;
    for feature_type in features {
        let id = match feature_type.id.clone() {
            Some(geojson::feature::Id::String(x)) => x,
            Some(geojson::feature::Id::Number(x)) => format!("{}", x),
            None => {
                log::error!("Failed to extract feature ID");
                continue;
            }
        };
        let source_id = format!("{}_{}_route", id, i);
        if ids.contains(&source_id) || i > 128 {
            continue;
        }
        i += 1;
        let feature = GeoJson::Feature(feature_type.clone());
        results.push(source_id.clone());
        map.add_geojson_source(
            source_id.clone(),
            feature
        )?;
        let color = [rand::random::<u8>(), rand::random::<u8>(), rand::random::<u8>()];
        let string = hex::encode(&color);
        let line_col = format!("#{}", string);
        
        log::info!("Adding layer");
        map.add_layer(&Layer {
            id: source_id.clone(),
            r#type: layer::LayerType::Line(LineLayerType { 
                line_color: line_col,
                line_width: 4,
            }),
            source: source_id,
            maxzoom: Some(15.0),
            minzoom: Some(4.0),
            layout: Some(layer::Layout {
                line_join: Some("round".into()),
                line_cap: Some("round".into()),
                icon_image: None,
                icon_size: None,
            }),
            paint: None,
            rendering_mode: Some(String::from("2d")),
            source_layer: None,
        })?;
    }
    Ok(results)
}

pub fn render_listings(listings: &HashMap<String, Vec<Listing>>, map: &Rc<Map>) {
    for (_key, val) in listings.iter() {
        for listing in val.iter() {
            let mut options = MarkerOptions::new();
            options.draggable = Some(false);
            let lng = listing.listing.property_details.longitude;
            let lat = listing.listing.property_details.latitude;
            let listing_marker = mapboxgl::Marker::with_options(LngLat::new(lng, lat), options);
            map.add_marker(listing_marker);

            let popup = mapboxgl::Popup::new(LngLat::new(lng, lat), PopupOptions {
                class_name: Some(String::from("sitePopUp")),
            });
            popup.set_html(format!("<p>{}<br />{}</p>", listing.listing.headline, listing.listing.price_details.display_price));
            popup.add_to(&map);
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bn() {
        let color = [rand::random::<u8>(), rand::random::<u8>(), rand::random::<u8>()];
        let string = hex::encode(&color);
        println!("#{}", string);
    }

    #[test]
    fn test_parsing() {
        let rfeat = get_suburb_features();
        assert!(rfeat.is_ok(), "{}", rfeat.unwrap_err());
        let features = rfeat.unwrap();
        let length = features.len();
        assert_ne!(length, 0);
        let mut count = 0;
        for feature in features {
            let feat: Feature = feature.into();
            assert!(feat.geometry.is_some());
            count += 1;
        }
        assert_eq!(count, length);
    }
}