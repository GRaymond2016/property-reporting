import { Map, type FillLayer, type FillPaint } from "mapbox-gl";
import 'mapbox-gl/dist/mapbox-gl.css';
import "../app.css";
import { invoke } from '@tauri-apps/api/tauri';
import type { TSPoint } from "../../src-tauri/bindings/TSPoint";
import type { TSSuburbOutline } from "../../src-tauri/bindings/TSSuburbOutline";

export class Elements {
    mapContainer = document.createElement('div');
    errorMessage = document.createElement('div');  
};

let elements: Elements;

let map: Map
let lng: number, lat: number, zoom: number;

const MAPBOX_API_KEY: string = "pk.eyJ1IjoiZ3JlZ3JheW1vbmQiLCJhIjoiY2xqa3YzYzk3MGtjMDNnbDUzb3FobWdzcyJ9.FbamrWM8y1r_znDW1L6Hxg";
lng = 151.20732; 
lat = -33.86785;
zoom = 9;

function setContext(el: Elements) {
    elements = el
}

function updateData() {
    const bounds = map.getBounds();
    const ne: TSPoint = {
        lat: bounds.getNorth(),
        lng: bounds.getEast()
    };
    const sw: TSPoint = {
        lat: bounds.getSouth(),
        lng: bounds.getWest()
    };
    console.log("invoking get_suburbs...");
    var price_slide = (document.getElementById("price-slide") as HTMLInputElement)
    invoke<TSSuburbOutline[]>('get_suburbs', { ne: ne, sw: sw, cost: price_slide.valueAsNumber * 1000} )
    .then((suburbOutline: TSSuburbOutline[]) => {
        console.log("iterating suburbOutlines");
        suburbOutline.forEach((x) => {
        const outline = x.outline.map((item) => {
            return [item.lng, item.lat];
        });
        try {
            if (map.isSourceLoaded(x.title)) {
            let fill = map.getLayer(x.title + '_fill') as FillLayer;
            if (fill.paint?.["fill-color"] != x.colour) {
                let paint = fill.paint as FillPaint;
                paint["fill-color"] = x.colour
                map.removeLayer(x.title + '_fill');
                map.addLayer({
                'id': x.title + '_fill',
                'type': 'fill',
                'source': x.title, // reference the data source
                'layout': {},
                'paint': {
                    'fill-color': x.colour,
                    'fill-opacity': 0.5,
                }
                });
            }
            return;
            }
        } catch {}
        const props = JSON.parse(x.properties);
        map.addSource(x.title, {
            'type': 'geojson',
            'data': {
            'type': 'Feature',
            'geometry': {
                'type': 'Polygon',
                'coordinates': [outline]
            },
            'properties': {
                "title": props["nsw_loca_2"] ?? x.title
            }
            }
        });
        map.addLayer({
            'id': x.title + '_fill',
            'type': 'fill',
            'source': x.title, // reference the data source
            'layout': {},
            'paint': {
            'fill-color': x.colour,
            'fill-opacity': 0.5,
            }
        });
        map.addSource(x.title + "_text_source", {
            'type': 'geojson',
            'data': {
            'type': 'Feature',
            'geometry': {
                'type': 'Point',
                'coordinates': [x.center.lng, x.center.lat]
            },
            'properties': {
                "title": props["nsw_loca_2"] ?? x.title
            }
            }
        });
        map.addLayer({
            'id': x.title + '_text',
            'type': 'symbol',
            'source': x.title + '_text_source', // reference the data source
            'layout': {
            "text-field": ["get", "title"],
            "text-rotation-alignment": "auto",
            "text-allow-overlap": true,
            "text-anchor": "top"
            },
            'paint': {
            "text-color": '#000000',
            "text-opacity": 0.2,
            }
        });
        })
    })
    .catch((err) => {
        const element = document.createElement('div');
        element.className="err";
        element.innerHTML=err;
        elements.errorMessage = element;
    });
}

function refresh() {
    invoke<[number, number, number]>('get_initial_state')
    .then(([lng, lat, zoom]) => {
        map.flyTo({center: [lng, lat], zoom: zoom});
        (document.getElementById("price-slide") as HTMLInputElement).valueAsNumber = 650;
    })
    .catch((err) => {
        const element = document.createElement('div');
        element.className="err";
        element.innerHTML=err;
        elements.errorMessage = element;
    })
}

function mount() {
    map = new Map({
        container: elements.mapContainer,
        accessToken: MAPBOX_API_KEY,
        style: `mapbox://styles/mapbox/outdoors-v11`,
        center: [lng, lat],
        zoom: zoom,
    });
    map.on('zoomend', () => {
        updateData();
    });
    map.on('moveend', () => {
        updateData();
    });
}

export {
    MAPBOX_API_KEY,
    setContext,
    refresh,
    updateData,
    mount,
    map,
}