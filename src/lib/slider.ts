import { updateData } from "./suburb_layers"

export function load() {
    var dol_label = (document.getElementById("price-in-dollars-label") as HTMLLabelElement)
    var price_slide = (document.getElementById("price-slide") as HTMLInputElement)

    price_slide.onchange = function() {
        if (dol_label == null || price_slide == null) {
            return
        }
        
        dol_label.innerHTML = ("$" + (price_slide.valueAsNumber * 1000))
        console.log("updateData")
        updateData()
    }
}