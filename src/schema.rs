// @generated automatically by Diesel CLI.

pub mod sql_types {
    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "geometry"))]
    pub struct Geometry;
}

diesel::table! {
    ingested (id) {
        id -> Int4,
        unique_source_name -> Text,
        data_hash -> Text,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::Geometry;

    sa4_2021_aust_gda2020 (gid) {
        gid -> Int4,
        sa4_code21 -> Nullable<Varchar>,
        sa4_name21 -> Nullable<Varchar>,
        chg_flag21 -> Nullable<Varchar>,
        chg_lbl21 -> Nullable<Varchar>,
        gcc_code21 -> Nullable<Varchar>,
        gcc_name21 -> Nullable<Varchar>,
        ste_code21 -> Nullable<Varchar>,
        ste_name21 -> Nullable<Varchar>,
        aus_code21 -> Nullable<Varchar>,
        aus_name21 -> Nullable<Varchar>,
        areasqkm21 -> Nullable<Float8>,
        loci_uri21 -> Nullable<Varchar>,
        geom -> Nullable<Geometry>,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::Geometry;

    school_rankings (id) {
        id -> Int4,
        school_name -> Text,
        school_location -> Nullable<Geometry>,
        rank_2021 -> Int4,
        rank_2020 -> Int4,
        success_rate_2021 -> Float8,
    }
}

diesel::table! {
    school_suspensions (id) {
        id -> Int4,
        region_name -> Text,
        total_short_suspensions -> Int4,
        total_number_of_students_short_suspended -> Int4,
        students_short_suspended_as_perc_of_group -> Float8,
        year_of_data -> Int4,
    }
}

diesel::table! {
    spatial_ref_sys (srid) {
        srid -> Int4,
        #[max_length = 256]
        auth_name -> Nullable<Varchar>,
        auth_srid -> Nullable<Int4>,
        #[max_length = 2048]
        srtext -> Nullable<Varchar>,
        #[max_length = 2048]
        proj4text -> Nullable<Varchar>,
    }
}

diesel::table! {
    suburb_prices (id) {
        id -> Int4,
        suburbs_query_id -> Int4,
        suburb_name -> Text,
        postcode -> Nullable<Text>,
        median_house_price -> Nullable<Int4>,
        median_unit_price -> Nullable<Int4>,
        year -> Nullable<Int4>,
        month -> Nullable<Int4>,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::Geometry;

    suburbs (id) {
        id -> Int4,
        name -> Text,
        outline -> Geometry,
        center -> Nullable<Geometry>,
        properties -> Jsonb,
    }
}

diesel::joinable!(suburb_prices -> suburbs (suburbs_query_id));

diesel::allow_tables_to_appear_in_same_query!(
    ingested,
    sa4_2021_aust_gda2020,
    school_rankings,
    school_suspensions,
    spatial_ref_sys,
    suburb_prices,
    suburbs,
);
