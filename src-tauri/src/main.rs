// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

mod geocoding;
mod data_types;
mod data_store;

use geocoding::lookup_address;
use tauri::Manager;
use tauri_plugin_log::LogTarget;
use std::env;

use diesel::prelude::*;
use postgis_diesel::types::{Point, Polygon};
use postgis_diesel::functions::st_intersects;
use base_lib::geo_models::{SuburbsQuery, SuburbPricesQuery};
use data_types::{TSSuburbOutline, TSPoint};
use data_store::*;

#[tauri::command]
fn get_initial_state() -> Result<[f64; 3], String> {
  let location = match lookup_address(String::from("17 Cusack Cl, St Helens Park NSW")) {
    Ok(x) => x,
    Err(e) => {
        let err = std::format!("Error occurred {:?}", e);
        log::error!("{}", err);
        return Err(err);
    }
  };
  Ok([location.longitude, location.latitude, 15.0])
}

fn new_point(x: f64, y: f64) -> Point {
  Point::new(x, y, Some(4326))
}

#[tauri::command]
fn get_suburbs(ne: TSPoint, sw: TSPoint, cost: i32) -> Result<Vec<TSSuburbOutline>, String> {
  log::info!("get_suburbs({:?}, {:?})", ne, sw);

  use base_lib::geo_schema::suburbs;
  use base_lib::geo_schema::suburbs::outline;
  use base_lib::geo_schema::suburb_prices;
  let north_east = ne;
  let south_west = sw;

  let mut bounding_box = Polygon::new(Some(4326));
  bounding_box.add_points([
    new_point(north_east.lng, north_east.lat),
    new_point(north_east.lng, south_west.lat),
    new_point(south_west.lng, south_west.lat),
    new_point(south_west.lng, north_east.lat),
    new_point(north_east.lng, north_east.lat)
  ]);

  let mut connection = connect_database();
  let res = suburbs::table
    .filter(st_intersects(
      bounding_box,
      outline
    ))
    .left_join(suburb_prices::table)
    .select((SuburbsQuery::as_select(), Option::<SuburbPricesQuery>::as_select()));
  let sql = diesel::debug_query::<diesel::pg::Pg, _>(&res);
  log::debug!("get_suburbs - sql: {:?}", sql);
  let results = match res.get_results::<(SuburbsQuery, Option<SuburbPricesQuery>)>(&mut connection) {
      Ok(x) => x,
      Err(e) => return Err(format!("{}", e))
    };
  log::debug!("get_suburbs - results: {:?}", results);
  let ts_results: Vec<TSSuburbOutline> = results.into_iter().map(|(suburb, suburb_price)|{
    let green = 80;
    let blue = 80;
    let closeness = match suburb_price {
      Some(x) => {
        if let Some(price) = x.median_house_price {
          let outer_limit = cost as f32 * 0.1;
          let diff = (cost - price).abs() as f32;
          let close = (diff / outer_limit).min(1.0f32);
          log::debug!("get_suburbs - closeness: {:?}", close);
          close
        } else { 0.0f32 }
      },
      None => {
        0.0f32
      }
    };


    return TSSuburbOutline {
      closeness: closeness,
      title: suburb.name.clone(),
      outline: suburb.outline.rings.iter().flatten().map(|x| {
                  TSPoint {
                    lng: x.x,
                    lat: x.y
                  }
                }).collect(),
      center: TSPoint { lng: suburb.center.x, lat: suburb.center.y },
      properties: serde_json::to_string(&suburb.properties).unwrap_or(String::from("")),
      colour: format!("#{:02X}{:02X}{:02X}", (closeness * 255f32) as i32, green, blue)
    };
  }).filter(|x| { x.closeness > 0.0001 }).collect();
  log::info!("get_suburbs() found {} results", ts_results.len());
  Ok(ts_results)
}

fn main() {
  let mut conn = connect_database();
  match check_static_sources_ingested(&mut conn) {
    Ok(_) => {},
    Err(e) => {
      log::error!("{}", e);
      panic!("Fatal error: {}", e);
    }
  };
  tauri::Builder::default()
    .plugin(tauri_plugin_log::Builder::default().targets([
      LogTarget::LogDir,
      LogTarget::Stdout,
      LogTarget::Webview,
    ]).build())
    .setup(|app| {
      #[cfg(debug_assertions)] // only include this code on debug builds
      {
        let window = app.get_window("main").unwrap();
        window.open_devtools();
      }
      Ok(())
    })
    .invoke_handler(tauri::generate_handler![get_initial_state, get_suburbs])
    .run(tauri::generate_context!())
    .expect("error while running tauri application");
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_ingest_data() {
        let mut conn = connect_database();
        match check_static_sources_ingested(&mut conn) {
            Ok(_) => {},
            Err(e) => {
            log::error!("{}", e);
            panic!("Fatal error: {}", e);
            }
        };
    }
}