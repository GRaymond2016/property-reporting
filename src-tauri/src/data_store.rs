use std::env;

use base_lib::conversions::convert_feature_collection_to_suburb_inserts;
use base_lib::ingested::{self, IngestedQuery, IngestedInsert};
use base_lib::geo_schema;
use diesel::{Connection, PgConnection, ExpressionMethods, BoolExpressionMethods, RunQueryDsl, SelectableHelper};
use dotenvy::dotenv;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

pub fn connect_database() -> PgConnection {
    dotenv().ok();

    match env::var("DATABASE_URL") {
        Ok(x) => {
            PgConnection::establish(&x).unwrap_or_else(|_| panic!("Error connecting to {}", x))
        }
        Err(e) => {
            log::error!("Error connecting database{}", e);
            panic!("Error connecting to database{}", e)
        }
    }
}

fn get_hash(data: &Vec<u8>) -> std::string::String {
    let mut hasher = DefaultHasher::new();
    data.hash(&mut hasher);
    format!("{:x}", hasher.finish())
}

fn is_already_ingested(connection: &mut PgConnection, data_source: &String, data: &Vec<u8>) -> Result<bool, Box<dyn std::error::Error>> {
    use crate::data_store::ingested::ingested::dsl::ingested;
    use crate::data_store::ingested::ingested::*;
    use diesel::query_dsl::methods::{SelectDsl, FilterDsl};

    let hash = get_hash(data);
    let query = unique_source_name.eq(data_source).and(data_hash.eq(hash));
    let result: Vec<IngestedQuery> = ingested
        .filter(query)
        .select(IngestedQuery::as_select())
        .get_results::<IngestedQuery>(connection)?;
    return Ok(result.len() > 0);
}

fn set_data_ingested(connection: &mut PgConnection, data_source: &String, data: &Vec<u8>) -> Result<(), Box<dyn std::error::Error>> {
    use crate::data_store::ingested::ingested::dsl::ingested;

    let insert = IngestedInsert {
        unique_source_name: data_source.clone(),
        data_hash: get_hash(data),
    };
    diesel::insert_into(ingested)
        .values(insert)
        .returning(IngestedQuery::as_returning())
        .get_result(connection)?;
    Ok(())
}

pub fn check_static_sources_ingested(connection: &mut PgConnection) -> Result<(), Box<dyn std::error::Error>> {
    use base_lib::geo_schema::suburbs::id;
    
    let path: String = String::from("../../base-lib/data/suburb-10-nsw.geojson");
    let data = include_bytes!("../../base-lib/data/suburb-10-nsw.geojson").to_vec();
    if !is_already_ingested(connection, &path, &data)? {
        use crate::data_store::geo_schema::suburbs::dsl::suburbs;
        let inserts = convert_feature_collection_to_suburb_inserts(std::str::from_utf8(&data)?)?;
        for insert in inserts {
            diesel::insert_into(suburbs)
                .values(insert)
                .returning(id)
                .get_result::<i32>(connection)?;
        }
        set_data_ingested(connection, &path, &data)?;
    }

    Ok(())
}
