use base_lib::AddressLookupResult;
use serde_json::Value;

const GEOCODE_API_URL: &str = "https://api.mapbox.com/geocoding/v5";
const GEOCODE_EP: &str = "/mapbox.places/";
const MAPBOX_API_KEY: &str = "pk.eyJ1IjoiZ3JlZ3JheW1vbmQiLCJhIjoiY2xqa3YzYzk3MGtjMDNnbDUzb3FobWdzcyJ9.FbamrWM8y1r_znDW1L6Hxg";

pub fn lookup_address(
    address_string: String,
) -> Result<AddressLookupResult, Box<dyn std::error::Error>> {
    let url: String = std::format!("{}{}{}{}{}", GEOCODE_API_URL, GEOCODE_EP, urlencoding::encode(&address_string), ".json?country=AU&access_token=", MAPBOX_API_KEY);
    let text = match reqwest::blocking::get(&url) {
        Ok(x) => {
            let data = x.bytes()?.to_vec();
            String::from_utf8(data)?
        },
        Err(e) => simple_error::bail!(e)
    };
    let json: Value = serde_json::from_str(text.as_str())?;

    let geom = json["features"].as_array().ok_or_else(|| simple_error::simple_error!("features is invalid array"))?;
    for elem in geom {
        let ob = elem.as_object().ok_or_else(|| simple_error::simple_error!("feature is invalid object"))?;
        let place_type = ob["place_type"].as_array().ok_or_else(|| simple_error::simple_error!("place_type is invalid array"))?;
        let rel = ob["relevance"].as_f64().ok_or_else(|| simple_error::simple_error!("relevance is invalid f64"))?;
        if place_type.contains(&Value::String(String::from("address"))) && rel > 0.65 {
            let center = ob["center"].as_array().ok_or_else(|| simple_error::simple_error!("center is invalid array"))?;
            let lng = match center.first() {
                Some(x) => x.as_f64().ok_or_else(|| simple_error::simple_error!("lng is invalid f64"))?,
                None => simple_error::bail!("lng doesn't exist")
            };
            let lat = match center.last() {
                Some(x) => x.as_f64().ok_or_else(|| simple_error::simple_error!("lat is invalid f64"))?,
                None => simple_error::bail!("lat doesn't exist")
            };
        
            let result = AddressLookupResult {
                address: String::from(url),
                latitude: lat,
                longitude: lng
            };
            log::info!("Result of address lookup {},{} for {}", lat, lng, address_string);
            return Ok(result);
        }
    }

    simple_error::bail!("Location not found");
}