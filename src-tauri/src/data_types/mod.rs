use ts_rs::TS;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, TS, Debug)]
#[ts(export)]
pub struct TSPoint {
    pub lng: f64,
    pub lat: f64
}

#[derive(Serialize, Deserialize, TS, Debug)]
#[ts(export)]
pub struct TSSuburbOutline {
    pub title: String,
    pub center: TSPoint,
    pub outline: Vec<TSPoint>,
    pub properties: String,
    pub colour: String,
    pub closeness: f32
}