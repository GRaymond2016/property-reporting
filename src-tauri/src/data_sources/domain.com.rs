const API_URL: &str = "https://api.domain.com.au/";
const RES_LISTING: &str = "v1/listings/residential/_search";

fn get_listing(
    listing_type: String,
    suburb: String,
    mut map: &HashMap<String, Vec<Listing>>
) -> ListingResult {
    let body = json!({
        "listingType":listing_type,
        "propertyTypes":[
            "House",
            "NewApartments"
        ],
        "minBedrooms":MIN_BEDS,
        "minBathrooms":MIN_BATH,
        "minCarspaces":0,
        "locations":[
            {
                "state":"ACT",
                "region":"",
                "area":"",
                "suburb":suburb,
                "postCode":"",
                "includeSurroundingSuburbs":false
            }
        ]
    }).to_string();

    let key = "key_83bf4618ff368bdd2d124ab4f648b884";
    let res = ehttp::Request {
        method: "POST".to_owned(),
        url: String::from(API_URL) + RES_LISTING,
        body: body.as_bytes().to_vec(),
        headers: ehttp::headers(&[
            ("Accept", "application/json"),
            ("Content-Type", "application/json"),
            ("X-API-Key", key)
        ]),
    };
    ehttp::fetch(res, move |result: ehttp::Result<ehttp::Response>| {
        match result {
            Ok(x) => {
                let body = match x.text() {
                    Some(x) => x,
                    None => {
                        log::error!("No body in response");
                        return;
                    },
                };
                let res = match serde_json::from_str::<Vec<Listing>>(body) {
                    Ok(x) => x,
                    Err(e) => {
                        log::error!("Error {:?} body was {}", e, body);
                        return;
                    },
                };
                map.insert(suburb.to_owned(), res.to_owned());
            },
            Err(e) => {
                log::error!("Error {:?}", e);
            }
        }

    });
    Ok(())
}
