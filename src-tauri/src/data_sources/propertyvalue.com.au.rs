
async fn get_suburb_insights(
    suburb_str: String,
) -> std::result::Result<(DemographicInfo, SuburbPriceData), Box<dyn std::error::Error>> {
    let caps = DesiredCapabilities::chrome();
    let driver = WebDriver::new("http://localhost:4444", caps).await?;

    driver
        .get(String::from("https://www.propertyvalue.com.au/suburb/") + suburb_str.as_str()).await?;
    let median_sale_price = driver.find_element(By::Id("metric-box-1")).await?.value().await?.unwrap_or_default();
    let properties_sold = driver.find_element(By::Id("metric-box-2")).await?.value().await?.unwrap_or_default();
    let median_rent = driver.find_element(By::Id("metric-box-3")).await?.value().await?.unwrap_or_default();
    let median_gross_yield = driver.find_element(By::Id("metric-box-4")).await?.value().await?.unwrap_or_default();
    let average_days_on_market = driver.find_element(By::Id("metric-box-5")).await?.value().await?.unwrap_or_default();
    let average_vendor_discount = driver.find_element(By::Id("metric-box-6")).await?.value().await?.unwrap_or_default();
    let median_price_change_1y = driver.find_element(By::Id("metric-box-7")).await?.value().await?.unwrap_or_default();

    let price = SuburbPriceData {
        median_sale_price_house: median_sale_price.replace("K", "000").replace("M", "000000").parse()?,
        median_price_change_1y_percent: median_price_change_1y.replace("%", "").parse::<f32>()?,
        average_days_on_market: average_days_on_market.parse()?,
        properties_sold: properties_sold.parse()?,
        average_discounting: average_vendor_discount.parse()?,
        median_rent: median_rent.parse()?,
        median_gross_yield: median_gross_yield.replace("%", "").parse::<f32>()?,
    };

    //TODO: Doesn't work yet: https://stackoverflow.com/questions/39305877/can-i-scrape-the-raw-data-from-highcharts-js, seems like wrong approach
    let _household_struct = driver.find_element(By::Id("highcharts-6"));
    let _household_occupancy = driver.find_element(By::Id("highcharts-17"));
    Ok((
        DemographicInfo {
            less_than_338k_household_income_percent: 0,
            greater_than_130k_household_income_percent: 0,
            home_owner_occupancy_percent: 0,
            couples_with_children_percent: 0,
        },
        price
    ))
}