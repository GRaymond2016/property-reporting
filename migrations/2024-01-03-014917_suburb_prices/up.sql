-- Your SQL goes here

CREATE TABLE suburb_prices
(
    id SERIAL PRIMARY KEY,
    suburbs_query_id SERIAL NOT NULL REFERENCES suburbs (id),
    suburb_name TEXT UNIQUE NOT NULL,
    postcode TEXT,
    median_house_price int,
    median_unit_price int,
    year int,
    month int
);

INSERT INTO suburb_prices (suburbs_query_id, suburb_name, postcode, median_unit_price, year, month) VALUES
((select id from suburbs where properties->>'nsw_loca_2' = UPPER('Eastlakes') limit 1), 'Eastlakes', '2018', 640000, 2023, 06),
((select id from suburbs where properties->>'nsw_loca_2' = UPPER('Hillsdale') limit 1), 'Hillsdale', '2036',	665000, 2023, 06),
((select id from suburbs where properties->>'nsw_loca_2' = UPPER('Greenwich') limit 1), 'Greenwich', '2065',	734000, 2023, 06),
((select id from suburbs where properties->>'nsw_loca_2' = UPPER('Ashfield') limit 1), 'Ashfield', '2131',	735000, 2023, 06),
((select id from suburbs where properties->>'nsw_loca_2' = UPPER('Canterbury') limit 1), 'Canterbury', '2193',	650000, 2023, 06);

INSERT INTO suburb_prices (suburbs_query_id, suburb_name, postcode, median_house_price, year, month) VALUES
((select id from suburbs where properties->>'nsw_loca_2' LIKE '%HELEN%PARK%' limit 1), 'St Helen''s Park', '2560',	760000, 2023, 06),
((select id from suburbs where properties->>'nsw_loca_2' = UPPER('North St Marys') limit 1), 'North St Marys', '2760',	765000, 2023, 06),
((select id from suburbs where properties->>'nsw_loca_2' = UPPER('Hebersham') limit 1), 'Hebersham', '2770',	725000, 2023, 06),
((select id from suburbs where properties->>'nsw_loca_2' = UPPER('Fairfield') limit 1), 'Fairfield', '2165',	975000, 2023, 06),
((select id from suburbs where properties->>'nsw_loca_2' = UPPER('Jamisontown') limit 1), 'Jamisontown', '2566',	866000, 2023, 06);