CREATE TABLE IF NOT EXISTS sa4_2021_aust_gda2020
(
    gid serial primary key,
    sa4_code21 character varying COLLATE pg_catalog."default",
    sa4_name21 character varying COLLATE pg_catalog."default",
    chg_flag21 character varying COLLATE pg_catalog."default",
    chg_lbl21 character varying COLLATE pg_catalog."default",
    gcc_code21 character varying COLLATE pg_catalog."default",
    gcc_name21 character varying COLLATE pg_catalog."default",
    ste_code21 character varying COLLATE pg_catalog."default",
    ste_name21 character varying COLLATE pg_catalog."default",
    aus_code21 character varying COLLATE pg_catalog."default",
    aus_name21 character varying COLLATE pg_catalog."default",
    areasqkm21 double precision,
    loci_uri21 character varying COLLATE pg_catalog."default",
    geom geometry(MultiPolygon,7844)
);

ALTER TABLE IF EXISTS sa4_2021_aust_gda2020
    OWNER to postgis;

CREATE INDEX IF NOT EXISTS sa4_2021_aust_gda2020_geom_geom_idx
    ON public.sa4_2021_aust_gda2020 USING gist
    (geom)
    TABLESPACE pg_default;