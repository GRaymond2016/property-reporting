CREATE EXTENSION if not exists postgis;

CREATE TABLE school_rankings (
    id SERIAL PRIMARY KEY,
    school_name TEXT NOT NULL,
    school_location geometry default (NULL),
    rank_2021 int NOT NULL,
    rank_2020 int NOT NULL,
    success_rate_2021 float NOT NULL
);

INSERT INTO school_rankings (school_name, rank_2021, rank_2020, success_rate_2021) VALUES 
('James Ruse Agricultural High School',1,1,70.58),
('North Sydney Boys High School',2,3,59.29),
('Baulkham Hills High School',3,2,58.24),
('Sydney Grammar School',4,5,56.13),
('Sydney Girls High School',5,9,53.23),
('Reddam House',6,12,52.34),
('Northern Beaches Secondary College Manly Campus',7,13,51.99),
('Hornsby Girls High School',8,6,51),
('North Sydney Girls High School',9,4,49.25),
('Normanhurst Boys High School',10,7,49.15),
('Kambala',11,21,47.68),
('Meriden School',12,17,47.09),
('Sydney Boys High School',13,10,46.55),
('Conservatorium High School',14,8,43.92),
('Abbotsleigh',15,16,43.79),
('Ascham School',16,11,43.77),
('SCEGGS Darlinghurst',17,14,42.15),
('Moriah College',18,41,40.47),
('Girraween High School',19,27,39.43),
('Ravenswood School for Girls',20,58,38.73),
('Loreto Kirribilli',21,23,38.67),
('Fort Street High School',22,18,38.65),
('St Catherines School',23,29,38.42),
('Wenona School',24,15,38.26),
('Pymble Ladies College',25,31,36.67),
('Al-Faisal College',26,54,36.19),
('St Aloysius College',27,20,36.06),
('Kincoppal Rose Bay School of the Sacred Heart',28,56,34.81),
('St Clares College',29,86,33.74),
('Knox Grammar School',30,19,32.95),
('Alpha Omega Senior College',31,55,32.69),
('Al Noori Muslim School',32,32,32.47),
('Penrith High School',33,37,32.36),
('Tara Anglican School for Girls',34,28,31.82),
('Cranbrook School',35,36,31.71),
('Emanuel School',36,38,31.18),
('Tangara School for Girls',37,25,30.89),
('Loreto Normanhurst',38,26,30.3),
('MLC School',39,51,29.71),
('Hurlstone Agricultural High School',40,33,29.65),
('St Lukes Grammar School',41,24,29.55),
('Sydney Technical High School',42,42,29.17),
('Roseville College',43,35,29.14),
('Caringbah High School',44,30,28.95),
('St George Girls High School',45,39,27.76),
('Frensham School',46,67,27.62),
('Sefton High School',47,70,27.53),
('Presbyterian Ladies College Sydney',48,22,27.28),
('Queenwood',49,65,26.44),
('Merewether High School',50,50,26.21),
('The Kings School',51,34,25.7),
('Monte Sant Angelo Mercy College',52,53,25.69),
('Parramatta Marist High School',53,46,25.53),
('SHORE Sydney Church of England Grammar School',54,40,25.38),
('Barker College',55,47,25.13),
('Newcastle Grammar School',56,45,25),
('Smiths Hill High School',57,68,24.64),
('Brigidine College Randwick',58,49,24.62),
('Danebank An Anglican School for Girls',59,75,24.35),
('Chatswood High School',60,69,24.16),
('International Grammar School',61,96,23.22),
('Santa Sabina College',62,48,23.11),
('Bethany College',63,80,22.92),
('Saint Ignatius College',64,44,22.67),
('Willoughby Girls High School',65,59,22.59),
('Brigidine College St Ives',66,62,21.95),
('St Mary and St Minas Coptic Orthodox College',67,0,21.85),
('Masada College',68,113,21.75),
('St Ursulas College',69,77,21.64),
('St Vincents College',70,52,21.45),
('Arden Anglican School',71,79,21.43),
('Northern Beaches Secondary College Balgowlah Boys Campus',72,60,21.31),
('St Augustines College Sydney',73,66,20.67),
('Freeman Catholic College',74,110,20.46),
('Central Coast Grammar School',75,83,20.42),
('Marist College Kogarah',76,117,20.38),
('The Scots College',77,63,20.28),
('Emmanuel Anglican College',78,149,20.26),
('St Spyridon College',79,74,20.11),
('Prairiewood High School',80,181,19.82),
('Clancy Catholic College',81,214,19.26),
('Mount St Benedict College',82,126,19.25),
('St Marks Coptic Orthodox College',83,88,19.2),
('Macarthur Anglican School',84,78,19.07),
('Cerdon College',85,163,19.01),
('Gosford High School',86,72,19),
('Marist Sisters College Woolwich',87,128,18.96),
('MacKillop College Port Macquarie',88,175,18.79),
('Macquarie Fields High School',89,124,18.76),
('Marist Catholic College Penshurst',90,129,18.73),
('The Hills Grammar School',91,106,18.18),
('Amity College',92,119,18.1),
('Norwest Christian College',93,0,18.09),
('Newington College',94,179,17.64),
('Marist College Eastwood',95,105,17.35),
('Stella Maris College',96,100,17.31),
('Aquinas Catholic College',97,102,17.24),
('Australian International School Malaysia',98,145,17.02),
('Our Lady of Mercy College Parramatta',99,81,16.67),
('St Columba Anglican School',100,139,16.64),
('William Clarke College',101,94,16.3),
('Newtown High School of Performing Arts',102,91,15.81),
('St George Christian School',103,107,15.81),
('St Charbels College',104,141,15.74),
('Malek Fahd Islamic School',105,152,15.73),
('Patrician Brothers College Fairfield',106,190,15.73),
('Mercy Catholic College',107,93,15.6),
('Cherrybrook Technology High School',108,90,15.53),
('Scots All Saints College',109,165,15.51),
('Cumberland High School',110,243,15.34),
('Blacktown Boys High School',111,272,15.05),
('Maronite College of the Holy Family Parramatta',112,316,14.97),
('Rosebank College',113,156,14.87),
('Mount St Patrick College',114,232,14.81),
('Oxford Falls Grammar School',115,73,14.78),
('Cheltenham Girls High School',116,84,14.75),
('St John Bosco College',117,158,14.69),
('St Philips Christian College Waratah',118,85,14.62),
('Pacific Hills Christian School',119,138,14.53),
('Our Lady of the Sacred Heart College',120,99,14.46),
('Holy Cross College',121,297,14.45),
('Engadine High School',122,201,14.45),
('Burwood Girls High School',123,82,14.43),
('Domremy Catholic College',124,220,14.41),
('Northern Beaches Secondary College Mackellar Girls Campus',125,43,14.4),
('SCECGS Redlands',126,151,14.32),
('Marcellin College',127,116,14.15),
('Tempe High School',128,166,13.91),
('St Patricks Marist College',129,174,13.89),
('St Ives High School',130,111,13.85),
('Xavier Catholic College Ballina',131,146,13.78),
('St Patricks College (Strathfield),',132,125,13.75),
('Carlingford High School',133,97,13.74),
('St Euphemia College',134,104,13.66),
('Covenant Christian School',135,134,13.65),
('Redeemer Baptist School',136,0,13.41),
('Canley Vale High School',137,279,13.4),
('Epping Boys High School',138,76,13.39),
('Mary MacKillop Catholic College',139,241,13.39),
('Rose Bay Secondary College',140,95,13.38),
('St Pius X College',141,114,13.34),
('Blakehurst High School',142,176,13.24),
('All Saints Grammar',143,233,13.23),
('Menai High School',144,164,13.08),
('Blacktown Girls High School',145,203,13.05),
('Al Amanah College',146,0,13.04),
('Killarney Heights High School',147,87,12.79),
('St Josephs College (Hunters Hill),',148,71,12.73),
('Blue Mountains Grammar School',149,115,12.65),
('Bonnyrigg High School',150,221,12.62);