CREATE EXTENSION if not exists postgis;

CREATE TABLE suburbs (
    id SERIAL PRIMARY KEY,
    name TEXT UNIQUE NOT NULL,
    outline geometry NOT NULL,
    center geometry GENERATED ALWAYS AS (ST_Centroid(outline)) STORED,
    properties jsonb NOT NULL
);