-- Your SQL goes here
CREATE TABLE school_suspensions (
    id SERIAL PRIMARY KEY,
    region_name TEXT NOT NULL,
    total_short_suspensions int NOT NULL,
    total_number_of_students_short_suspended int NOT NULL,
    students_short_suspended_as_perc_of_group float NOT NULL,
    year_of_data int NOT NULL
);

INSERT INTO
    school_suspensions (
        region_name,
        total_short_suspensions,
        total_number_of_students_short_suspended,
        students_short_suspended_as_perc_of_group,
        year_of_data
    )
VALUES
    ('Sydney-North', 747, 640, 0.7, 2021),
    ('Sydney-Inner', 1331, 1007, 1.8, 2021),
    ('Sydney-South', 2512, 1893, 2.5, 2021),
    ('Sydney-West', 3428, 2412, 2.6, 2021),
    ('North East NSW', 6462, 3944, 6.5, 2021),
    ('South West NSW', 4417, 2748, 5.3, 2021),
    ('Central Coast, Newcastle', 4891, 3314, 4.2, 2021),
    ('Sydney-South West', 4173, 3102, 3.4, 2021),
    ('Sydney-North West', 1925, 1419, 2.1, 2021),
    ('North West NSW', 7537, 4622, 7.1, 2021),
    ('South East NSW', 5613, 3524, 4.9, 2021);