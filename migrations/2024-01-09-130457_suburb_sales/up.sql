-- Schema defined here:
-- https://www.valuergeneral.nsw.gov.au/__data/assets/pdf_file/0015/216402/Current_Property_Sales_Data_File_Format_2001_to_Current.pdf
-- Download data from: https://valuation.property.nsw.gov.au/embed/propertySalesInformation
CREATE TABLE suburb_sales {
    id SERIAL PRIMARY KEY,
    district_code TEXT NOT NULL,
    property_id TEXT NOT NULL,
    sale_counter TEXT NOT NULL,
    download_datetime DATETIME NOT NULL,
    property_name TEXT NOT NULL,
    property_unit_number TEXT NOT NULL,
    property_house_number TEXT NOT NULL,
    property_street_name TEXT NOT NULL,
    property_locality TEXT NOT NULL,
    property_post_code TEXT NOT NULL,
    area TEXT NOT NULL,
    area_type TEXT NOT NULL,
    contract_date DATETIME NOT NULL,
    settlement_date DATETIME NOT NULL,
    purchase_price BIGINT NOT NULL,
    zoning TEXT NOT NULL,
    nature_of_property TEXT NOT NULL,
    primary_purpose TEXT NOT NULL,
    strata_lot_number TEXT NOT NULL,
    component_code TEXT NOT NULL,
    sale_code TEXT NOT NULL,
    percent_interest_of_sale TEXT NOT NULL,
    dealing_number TEXT NOT NULL
};