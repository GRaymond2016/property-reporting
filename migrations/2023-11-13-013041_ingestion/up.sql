CREATE TABLE ingested (
    id SERIAL PRIMARY KEY,
    unique_source_name TEXT NOT NULL,
    data_hash TEXT NOT NULL
);